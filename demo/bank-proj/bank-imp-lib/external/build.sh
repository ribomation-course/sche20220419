#!/usr/bin/env bash
set -eux

OPTS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"

rm -rf objs
mkdir objs
g++ $OPTS -c src/util.cxx -o objs/util.o
g++ $OPTS -c src/account.cxx -o objs/account.o

rm -rf dist
mkdir dist
ar cqs dist/libaccounts.a objs/util.o objs/account.o
cp src/account.hxx dist

