#pragma once
#include <iosfwd>
#include <string>

namespace ribomation {
	using std::string;
	using std::ostream;

	class Account {
		string accno;
		int balance;
	public:
		Account(string accno_, int balance_ = 0);
		int getBalance() const;
		int update(int amount);
		friend auto operator <<(ostream& os, Account const& a) -> ostream&;
	};
}
