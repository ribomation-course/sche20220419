#include <iostream>
#include "account.hxx"
#include "util.hxx"
using namespace std;

namespace ribomation {
	Account::Account(string accno_, int balance_) 
		: accno{ toUpperCase(move(accno_)) }, balance{balance_} 
	{ }

	int Account::getBalance() const { return balance; }

	int Account::update(int amount) {
		balance += amount;
		return balance;
	}

	auto operator <<(ostream& os, Account const& a) -> ostream& {
		return os << "Account<" << a.accno << ", " << a.balance << " kr>";
	}
}
