cmake_minimum_required(VERSION 3.18)
project(bank LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_subdirectory(external)
add_subdirectory(lib)
add_subdirectory(app)

add_custom_target(run
    COMMAND bank --help
    COMMAND echo "----"
    COMMAND bank
    COMMAND echo "----"
    COMMAND bank --count=15
)
