#include <iostream>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "account.hxx"
#include "util.hxx"

using std::string;
using std::ostream;
namespace uuid = boost::uuids;

namespace ribomation {
	Account::Account(string accno_, int balance_) 
		: accno{ toUpperCase(move(accno_)) }, balance{balance_} 
	{ 
		auto gen = uuid::random_generator{};
		id = to_string( gen() );
	}

	string Account::getId() const { return id; }
	string Account::getAccno() const { return accno; }
	int Account::getBalance() const { return balance; }

	int Account::update(int amount) {
		balance += amount;
		return balance;
	}

	auto operator <<(ostream& os, Account const& a) -> ostream& {
		return os << "Account<" << a.id << ", " << a.accno << ", " << a.balance << " kr>";
	}
}
