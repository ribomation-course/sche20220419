#pragma once
#include <string>
#include <algorithm>
#include <cctype>
#include "util.hxx"

inline auto toUpperCase(std::string s) -> std::string {
	std::transform(s.begin(), s.end(), s.begin(), [](auto ch){
		return ::toupper(ch);		
	});
	return s;
}
