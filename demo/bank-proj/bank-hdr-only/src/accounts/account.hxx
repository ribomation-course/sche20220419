#pragma once
#include <iosfwd>
#include <string>
#include "util.hxx"

namespace ribomation {
	using std::string;
	using std::ostream;

	class Account {
		string accno;
		int balance;
	public:
		Account(string accno_, int balance_ = 0) 
			: accno{ toUpperCase(move(accno_)) }, balance{balance_} 
		{ }

		int getBalance() const { return balance; }

		int update(int amount) {
			balance += amount;
			return balance;
		}

		friend auto operator <<(ostream& os, Account const& a) -> ostream& {
			return os << "Account<" << a.accno << ", " << a.balance << " kr>";
		}
	};
}
