cmake_minimum_required(VERSION 3.18)
project(bank LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 17)

# -- accounts lib --
#add_library(accounts STATIC)
add_library(accounts OBJECT)
#add_library(accounts SHARED)
#set_target_properties(accounts PROPERTIES
#    VERSION     1.2.3
#    SOVERSION   1
#)
target_sources(accounts
    PRIVATE src/accounts/api/account.hxx
            src/accounts/impl/account.cxx
            src/accounts/impl/util.hxx
            src/accounts/impl/util.cxx
)
target_include_directories(accounts
    PRIVATE src/accounts/impl
    PUBLIC  src/accounts/api
)
target_compile_options(accounts 
    PUBLIC -Wall -Wextra -Werror -Wfatal-errors
)

# -- bank exe --
add_executable(bank)
target_sources(bank
    PRIVATE src/bank/bank.cxx
)
target_link_libraries(bank PRIVATE accounts)

# -- run --
add_custom_target(run 
    COMMAND bank
)
