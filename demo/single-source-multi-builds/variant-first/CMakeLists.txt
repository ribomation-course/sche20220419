
add_executable(app-1st "")

target_sources(app-1st PRIVATE 
    ${SRC_COMMON}
)

target_compile_definitions(app-1st PRIVATE
    VARIANT=1
)

set_target_properties(app-1st PROPERTIES 
    OUTPUT_NAME app
)

