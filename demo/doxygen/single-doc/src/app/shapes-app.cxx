#include <iostream>
#include "shape-generator.hxx"

using namespace std;
using namespace ribomation::shapes;

void print(Shape& s) {
    cout << s << endl;
}

int main(int numArgs, char* args[]) {
    int N = (numArgs <= 1) ? 5 : stoi(args[1]);
    ShapeGenerator gen;
    auto shapes = gen.mkShapes(N);

    cout << "--- Shapes ---\n";
    for (auto s : shapes) { print(*s); }
    cout << "--- o0o ---\n";

    while (!shapes.empty()) {
        auto s = shapes.back();
        shapes.pop_back();
        delete s;
    }

    return 0;
}
