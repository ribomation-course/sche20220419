#include "rect.hxx"
#include <sstream>
#include <iostream>

using namespace std;

namespace ribomation::shapes {
    Rect::Rect(const string& type, int width, int height)
            : super{type}, width{(width)}, height{height} {
        cout << type << "{" << width << ", " << height << "} @ " << this << endl;
    }

    Rect::~Rect() {
        cout << "~Rect() @ " << this << endl;
    }

    double Rect::area() const {
        return width * height;
    }

    string Rect::toString() const {
        ostringstream buf;
        buf << super::toString() << "{" << width << ", " << height << "}";
        return buf.str();
    }

}
