#pragma once

#include <cmath>
#include "shape.hxx"

namespace ribomation::shapes {
    class Circle : public Shape {
        using super = Shape;
        const double PI = 4 * atan(1);
        int          radius;
    public:
        explicit Circle(int radius);
        ~Circle() override;
        double area() const override;
        std::string toString() const override;
    };
}