#pragma once

#include <string>
#include "shape.hxx"

namespace ribomation::shapes {
    class Rect : public Shape {
        using super = Shape;
        int width, height;
    protected:
        Rect(const std::string& type, int width, int height);
    public:
        Rect(int width, int height) : Rect("rect", width, height) {}

        ~Rect() override;
        double area() const override;
        std::string toString() const override;
    };
}
