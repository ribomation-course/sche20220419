#pragma once

#include "shape.hxx"

namespace ribomation::shapes {
    class Triangle : public Shape {
        using super = Shape;
        int base, height;
    public:
        Triangle(int base, int height);
        ~Triangle() override;
        double area() const override;
        std::string toString() const override;
    };
}
