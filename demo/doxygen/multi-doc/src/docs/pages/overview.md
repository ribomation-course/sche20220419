# Shapes App    {#mainpage}
## Version 42

This is just a simple C++ OOP demo app, used as compilation and
build target for CMake.

Use it as is, without any guaranties or promises. 

Delivered to you in shame by _Bugsify Ltd_.

# Usage

~~~{.cpp}
ShapeGenerator gen;
auto shapes = gen.mkShapes(N);
for (auto s : shapes) std::cout << *s << "\n";
for (auto s : shapes) delete s;
~~~

# Modules

* [Shapes Lib](./shapes/index.html)
* [Generator Lib](./generator/index.html)
* [Main App](./main/index.html)

