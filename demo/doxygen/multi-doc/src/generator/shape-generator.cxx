#include <string>
#include <random>
#include <vector>
#include <stdexcept>
#include "shape.hxx"
#include "rect.hxx"
#include "circle.hxx"
#include "triangle.hxx"
#include "square.hxx"

#include "shape-generator.hxx"

using namespace std;
using namespace std::literals;

namespace ribomation::shapes {
    Shape* ShapeGenerator::mkShape() {
        static random_device          r;
        uniform_int_distribution<int> nextShape{1, 4};
        uniform_int_distribution<int> nextValue{1, 10};

        switch (nextShape(r)) {
            case 1:
                return new Rect{nextValue(r), nextValue(r)};
            case 2:
                return new Circle{nextValue(r)};
            case 3:
                return new Triangle{nextValue(r), nextValue(r)};
            case 4:
                return new Square{nextValue(r)};
            default:
                break;
        }
        throw domain_error("WTF: this should not happen");
    }

    vector<Shape*> ShapeGenerator::mkShapes(int n) {
        vector<Shape*> shapes;
        while (n-- > 0) shapes.push_back(mkShape());
        return shapes;
    }
}
