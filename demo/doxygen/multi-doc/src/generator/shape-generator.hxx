#pragma once
#include <vector>
#include "shape.hxx"
#include "rect.hxx"
#include "circle.hxx"
#include "triangle.hxx"
#include "square.hxx"

namespace ribomation::shapes {
    /// Generator for one or many shapes.
    ///
    /// Can either generate a single randomly chosen pointer to a shape object,
    /// that means, a subclass object to Shape. 
    /// Or, it can generate a `std::vector` of heap allocated shape objects.
    /// ~~~{.cpp}
    /// using ribomation::shapes::ShapeGenerator;
    /// auto gen = ShapeGenerator();
    /// auto s = gen.mkShape();
    /// std::cout << *s << "\n";
    /// delete s;
    /// ~~~
    struct ShapeGenerator {
        Shape* mkShape();
        std::vector<Shape*> mkShapes(int numShapes);
    };
}

