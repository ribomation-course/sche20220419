cmake_minimum_required(VERSION 3.10)
project(msg LANGUAGES CXX)


enable_testing()

add_test(NAME bash
    COMMAND ${CMAKE_SOURCE_DIR}/src/dummy-test.sh
)

add_executable(cxx-test src/dummy-test.cxx)
add_test(NAME cxx
    COMMAND cxx-test
)

set(CATCH2_DIR ${CMAKE_BINARY_DIR}/catch2)
set(CATCH2_FILE ${CATCH2_DIR}/catch.hpp)
if(NOT EXISTS ${CATCH2_FILE})
    file(DOWNLOAD
        https://github.com/catchorg/Catch2/releases/download/v2.5.0/catch.hpp
        ${CATCH2_FILE}
        SHOW_PROGRESS
    )
endif()

add_executable(cxx-test2 
    src/catch2-test.cxx
)
target_include_directories(cxx-test2
    PRIVATE     ${CATCH2_DIR}
)
add_test(NAME cxx2
    COMMAND     cxx-test2
)

