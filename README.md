# CMake
### 19 - 20 April 2022

Welcome to this course that will get you up to speed with using CMake for C/C++ application builds.


# Links
* [Installation instructions](./installation-instructions.md)
* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/build-tools/cmake/)


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated.
Create a dedicated directory for this course and a sub-directory for
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/cmake-course/my-solutions
    cd ~/cmake-course
    git clone <git HTTPS clone link> gitlab

![GIT HTTPS URL](./img/git-url.png)

During the course, solutions will be push:ed to this repo, and you can get these by
a `git pull` operation

    cd ~/cmake-course/gitlab
    git pull


# Interesting Videos

* [Hello World from Scratch - Peter Bindels & Simon Brand [ACCU 2019]](https://youtu.be/MZo7k_IOCe8)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>


