# Build

    cmake -S . -B bld
    cmake --build bld
    cmake --build bld --target test
    ( cd bld && ctest )
    ( cd bld && make test )

# Clean

    rm -rf bld

