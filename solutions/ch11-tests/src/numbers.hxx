#pragma once

auto fibonacci(unsigned n) -> unsigned long;
auto sum(unsigned n) -> unsigned long;

