
# Shapes

    cmake -S ./1-shapes -B bld/shapes
    cmake --build bld/shapes
    cmake --build bld/shapes --target run

# SHM

    cmake -S 2-shm -B bld/shm
    cmake --build bld/shm
    cmake --build bld/shm --target run

# SHM - Header Only

    cmake -S 3-shm-hdr -B bld/shm-hdr
    cmake --build bld/shm-hdr
    cmake --build bld/shm-hdr --target run

# Clean

    rm -rf bld

