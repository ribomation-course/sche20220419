cmake_minimum_required(VERSION 3.12)
project(project-meta-data
	VERSION		1.2.3
    LANGUAGES	CXX
)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)


find_package(Git REQUIRED)
execute_process(
  COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
  OUTPUT_STRIP_TRAILING_WHITESPACE
  OUTPUT_VARIABLE  GIT_COMMIT_ID
  RESULT_VARIABLE  git_failed
)
if(git_failed)
  message(FATAL_ERROR "Failed to invoke GIT: ${git_failed}")
endif()

foreach(key IN ITEMS HOSTNAME OS_NAME PROCESSOR_NAME)
		cmake_host_system_information(RESULT INFO_${key}  QUERY ${key})
endforeach()

set(ENV_USERNAME $ENV{USER})

configure_file(src/in/build-info.hxx.in 
	gen/build-info.hxx 	@ONLY
	NEWLINE_STYLE		UNIX
)


add_executable(app src/cxx/app.cxx)
target_include_directories(app
	PRIVATE  ${CMAKE_BINARY_DIR}/gen
)

add_custom_target(run 
		COMMAND app
)
