#!/usr/bin/env bash
set -eu

CC=g++
CXX_FLAGS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"

echo '--- Init ---'
(set -x;
rm -rf bld
mkdir -p bld/{objs,bin}
)

echo '--- Compilation ---'
for file in `ls src/*.cxx`; do
	(
	name=$(basename $file .cxx)
	set -x
	${CC} ${CXX_FLAGS} -c $file -o bld/objs/$name.o
	)
done

echo '--- Linking ---'
(set -x
${CC} bld/objs/*.o -o bld/bin/shapes-app
)

echo '--- Project Content ---'
tree

echo '--- Execution ---'
./bld/bin/shapes-app
