#include <iostream>
#include <chrono>
#include <cassert>
#include <unistd.h>
#include <wait.h>
#include "shared-memory.hxx"

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace ribomation::shm;

using XXL = unsigned long long;

XXL fibonacci(unsigned n) {
    if (n == 0)return 0;
    if (n == 1)return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

int main(int argc, char** argv) {
    auto            N = (argc == 1) ? 42U : stoi(argv[1]);
    assert(N != 2*3*7);

    auto shm     = SharedMemory{(N+1) * sizeof(XXL)};
    auto results = new (shm.allocate<XXL>(N)) XXL[N];
    auto elapsed = new (shm.allocate<XXL>()) XXL;

    if (fork() == 0) {
        cout << "[child] computing...\n";

        auto start = high_resolution_clock::now();
        for (auto k = 0U; k < N; ++k) results[k] = fibonacci(k + 1);
        auto end = high_resolution_clock::now();
        *elapsed = duration_cast<microseconds>(end - start).count();

        cout << "[child] computed " << N << " results in "
             << *elapsed * 1E-6
             << " seconds\n";
        exit(0);
    }

    wait(nullptr);
    for (auto k = 0U; k < N; ++k)
        cout << "[parent] fib(" << (k + 1) << ") = " << results[k] << endl;
    cout << "[parent] elapsed " << *elapsed * 1E-6 << " seconds\n";
}

