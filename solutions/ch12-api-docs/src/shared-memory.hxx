#pragma once

#include <string>

namespace ribomation::shm {
    using namespace std;
    using namespace std::literals;

    class SharedMemory {
        const string    name = "/myshm"s;
        const unsigned  size;
        const void*     begin;
        const void*     end;
        void* next;

        void sysfail(const string& func);

    public:
        SharedMemory(unsigned long size);
        ~SharedMemory();

        void* allocateBytes(unsigned numBytes);

        template<typename T>
        T* allocate(unsigned numElems=1) {
            return reinterpret_cast<T*>(allocateBytes(numElems * sizeof(T)));
        }

        SharedMemory() = delete;
        SharedMemory(const SharedMemory&) = delete;
        SharedMemory& operator=(const SharedMemory&) = delete;
    };

}
