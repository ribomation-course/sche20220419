# Build & Run
    cmake -S . -B bld
    cmake --build bld --target run
    cmake --build bld --target docs
    www-browser bld/html/index.html

# Clean
    rm -rf bld
