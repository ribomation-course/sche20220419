
# Build & Run

## Shapes
    cmake -S shapes -B bld/shapes
    cmake --build bld/shapes
    cmake --build bld/shapes --target run

## SHM
    cmake -S shared-mem -B bld/shm
    cmake --build bld/shm
    cmake --build bld/shm --target run

# Executables

    ./bld/shapes/shapes
    ./bld/shm/shm-app

# Clean
    rm -rf bld

