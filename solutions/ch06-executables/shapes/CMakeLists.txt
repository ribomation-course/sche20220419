cmake_minimum_required(VERSION 3.12)
project(shapes LANGUAGES CXX)

set(CMAKE_CXX_STANDARD			17)
set(CMAKE_CXX_STANDARD_REQUIRED	ON)
set(CMAKE_CXX_EXTENSIONS 		OFF)

add_executable(shapes)

target_sources(shapes PRIVATE
    ./src/hdrs/shape.hxx            ./src/cxx/shape.cxx 
    ./src/hdrs/rect.hxx             ./src/cxx/rect.cxx
    ./src/hdrs/square.hxx           ./src/cxx/square.cxx
    ./src/hdrs/triangle.hxx         ./src/cxx/triangle.cxx
    ./src/hdrs/circle.hxx           ./src/cxx/circle.cxx 
    ./src/hdrs/shape-generator.hxx ./src/cxx/shape-generator.cxx 
    ./src/cxx/shapes-app.cxx
)

target_include_directories(shapes PRIVATE ./src/hdrs/)

target_compile_options(shapes PRIVATE  
    -Wall -Wextra -Werror -Wfatal-errors)

add_custom_target(run
	COMMAND $<TARGET_FILE:shapes> 20
)
