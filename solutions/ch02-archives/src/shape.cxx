#include <iostream>
#include <sstream>
#include <string>
#include "shape.hxx"
using namespace std;

Shape::Shape(const string& type) : type(type) {
    cout << "Shape{" << type << "} @ " << this << endl;
}

Shape::~Shape() {
    cout << "~Shape() type=" << type << " @ " << this << endl;
}

string Shape::toString() const {
    ostringstream buf;
    buf << getType() << " area=" << area() << " ";
    return buf.str();
}

