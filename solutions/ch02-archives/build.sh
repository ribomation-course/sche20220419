#!/usr/bin/env bash
set -eu

CC=g++
AR=ar
CXX_FLAGS="-std=c++17 -Wall -Wextra -Werror -Wfatal-errors"

echo '--- Init ---'
(set -x;
rm -rf bld
mkdir -p bld/{objs,lib,bin}
)

echo '--- Compilation ---'
for file in `ls src/*.cxx`; do
	(
	name=$(basename $file .cxx)
	[ $name = "shapes-app" ] && continue
	set -x
	${CC} ${CXX_FLAGS} -c $file -o bld/objs/$name.o
	)
done

echo '--- Archive ---'
(set -x
${AR} crs bld/lib/shapes.a bld/objs/*.o
)

echo '--- Linking ---'
(set -x
${CC} ${CXX_FLAGS} -c src/shapes-app.cxx -o bld/objs/shapes-app.o
${CC} bld/objs/shapes-app.o bld/lib/shapes.a -o bld/bin/shapes-app
)
echo '--- Project Content ---'
tree

echo '--- Execution ---'
./bld/bin/shapes-app
