
# Build

    cmake -S . -B bld
    cmake --build bld
    cmake --build bld --target run

# Clean

    rm -rf bld
