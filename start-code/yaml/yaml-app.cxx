#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "yaml-cpp/yaml.h"
using namespace std;

int main(int argc, char** argv) {
    vector<string> tags = {"foo", "bar", "fee"};
    map<string, string> person = {
        {"name" , "Anna Conda"},
        {"age"  , "42"},
        {"color", "red"}
    };

    YAML::Emitter yaml{cout};
	yaml << YAML::BeginSeq 
			<< tags 
			<< person
		 << YAML::EndSeq;

		 return 0;
}
